<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calls_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getall() {
        $query = $this->db->get('calls');
         return $query->result_array();
    }

    public function getid($id) {
        if($id != FALSE) {
            $query = $this->db->get_where('calls', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function add($data) {
        if($this->db->insert('calls', $data) == TRUE)
            return 'Registro inserido com sucesso!';
        else
            return 'Erro ao inserir registro!';
    }

}

?>