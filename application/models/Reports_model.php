<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getall() {
        $query = $this->db->get('reports');
         return $query->result_array();
    }

    public function getid($id) {
        if($id != FALSE) {
            $query = $this->db->get_where('reports', array('id' => $id));
            $this->db->order_by("id", "desc");
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function add($data) {
        if($this->db->insert('reports', $data) == TRUE)
            return 'Registro inserido com sucesso!';
        else
            return 'Erro ao inserir registro!';
    }

}

?>