<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Problems_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function getall() {
        $query = $this->db->get('problems');
         return $query->result_array();
    }

}

?>