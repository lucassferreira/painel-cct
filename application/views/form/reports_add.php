<?php
$this->load->helper('form');

//print_r($data);

$impacts= array();
foreach ($data['impacts'] as $i){
    $impacts[$i['id']] = $i['description'];
};

$problems = array();
foreach ($data['problems'] as $i){
    $problems[$i['id']] = $i['description'];
};

//print_r($data);

echo form_open('reports/add', 
    array(
        'method'    => 'POST'
    )
);

echo form_input(
    array(
        'name'          => 'calls_id',
        'id'            => 'calls_id',
        'value'         => $data['calls']['id'],
        'readonly'     => 'true'
    )
);
echo '<br><br>';

echo form_dropdown('problems_id', $problems , '1');
echo '<br><br>';

echo form_dropdown('impacts_id', $impacts , '2');
echo '<br><br>';

echo form_input(
    array(
        'name'          => 'name',
        'id'            => 'name',
        'placeholder'   => 'Nome do atendente',
        'required'      =>  'required'
    )
);
echo '<br><br>';

echo form_textarea(
    array(
        'name'          => 'textreport',
        'id'            => 'textreport',
        'placeholder'   => 'Descrição do problema',
        'required'      =>  'required'
    )
);
echo '<br><br>';








echo form_submit('', 'Cadastrar');
echo form_close();


?>