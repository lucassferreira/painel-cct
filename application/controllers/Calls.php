<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calls extends CI_Controller {

	public function index(){
		$this->load->model('calls_model');
		$calls = $this->calls_model->getall();
        $data['data'] = $calls;
		$this->load->view('calls', $data);
    }

    public function add() {
        $this->load->model('calls_model');
		$return = $this->calls_model->add($_POST);
		print $return;
		$calls = $this->calls_model->getall();
        $data['data'] = $calls;
		$this->load->view('calls', $data);
    }
    

}