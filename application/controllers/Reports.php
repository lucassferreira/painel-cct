<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function index(){
		$this->load->model('reports_model');
		$reports = $this->reports_model->getall();
		$data['data'] = $reports;
		$this->load->view('reports', $data);
    }

    public function form_add($id) {
		$this->load->model('calls_model');
		$calls = $this->calls_model->getid($id);
        $data['data']['calls'] = $calls;

        $this->load->model('impacts_model');
		$impacts = $this->impacts_model->getall();
		$data['data']['impacts'] = $impacts;

        $this->load->model('problems_model');
		$problems = $this->problems_model->getall();
		$data['data']['problems'] = $problems;

        $this->load->view('form/reports_add',$data);
    }
    
    public function add() {
        $this->load->model('reports_model');
        $_POST['dt'] = date("Y-m-d H:i:s");
		$return = $this->reports_model->add($_POST);
        print $return;
		redirect('/');
    }


    

}