/*
Padrão de nomeclatura das bases
Nome das Classes: camelCase sempre no plural
Campos: camelCase sempre no singular
Chaves estrangeiras: Nome da tabela camelCase + underscore + nome do campo camelCase na tabela de origem. Exemplo: tabelaCamelCase_nomeDoCampoNaTabelaDeOrigem
Tabelas de relações N:N: devem conter os nomes das tabelas + underscore Ex: clietes_hosts_filas
1 -  Os campos devem ser descretivios e não abreviados quando possível.
*/

-------------------------------------------------------------------------

DROP TABLE IF EXISTS calls;
CREATE TABLE calls (
    id SERIAL NOT NULL UNIQUE PRIMARY KEY,
    origNumber varchar(20) NOT NULL,
    destNumber varchar(20) NOT NULL,
    dtstart timestamp NOT NULL,
    dtend timestamp NOT NULL,
    md5hash varchar(32) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS problems;
CREATE TABLE problems (
    id SERIAL NOT NULL UNIQUE PRIMARY KEY,
    description varchar(120) NOT NULL
);

DROP TABLE IF EXISTS impacts;
CREATE TABLE impacts (
    id SERIAL NOT NULL UNIQUE PRIMARY KEY,
    description varchar(120) NOT NULL
);

DROP TABLE IF EXISTS reports;
CREATE TABLE reports (
    id SERIAL NOT NULL UNIQUE PRIMARY KEY,
    name varchar(120) NOT NULL,
    textreport text,
    calls_id integer REFERENCES calls (id) ON DELETE CASCADE,
    problems_id integer REFERENCES problems (id) ON DELETE CASCADE,
    impacts_id integer REFERENCES impacts (id) ON DELETE CASCADE,
    dt timestamp NOT NULL
);

DROP TABLE IF EXISTS searchs;
CREATE TABLE searchs (
    id SERIAL NOT NULL UNIQUE PRIMARY KEY,
    number varchar(20) NOT NULL,
    score INT NOT NULL
);



--Registros

INSERT INTO public.problems ("description") VALUES('CHAMADA MUDA');
INSERT INTO public.problems ("description") VALUES('CHAMADA PICOTANDO');
INSERT INTO public.problems ("description") VALUES('CHAMADA COM RUÍDO/BARULHO');
INSERT INTO public.problems ("description") VALUES('CHAMADA COM ATRASO NA VOZ');
INSERT INTO public.problems ("description") VALUES('CHAMADA COM A FALA EMBOLADA');


INSERT INTO public.impacts ("description") VALUES('NÃO ATRAPALHOU');
INSERT INTO public.impacts ("description") VALUES('INCOMODOU');
INSERT INTO public.impacts ("description") VALUES('DIFICULTOU');
INSERT INTO public.impacts ("description") VALUES('IMPOSSIBILITOU');

-------------------------------------------------------------------------